INTRODUCTION
------------
This module is meant to be a simple wrapper for the Libav audio and video processing tools. It provides media conversion
and handling functionality through actions accessible via the Rules and/or VBO modules.

FEATURES
--------
 * Calculate audio/video duration (in seconds)
 * Create a thumbnail image from a video at a specified time offset
 * Audio and Video conversion
 * Rules and VBO integration

REQUIREMENTS
------------
 * The avconv executable should be installed and accessible on your web server.
 * Support for additional codecs / file formats may require external libraries to be compiled into your avconv executable.
 * Some way to expose the actions on the site (ex: Administration Views)

Consult your server administrator or hosting provider if you are unsure about these requirements.

SIMILAR MODULES
---------------
 * FFmpeg Wrapper / FFmpeg Converter
 * Media Mover
 * Video
