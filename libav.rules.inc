<?php
/**
 * @file
 * Rules actions for Libav audio/video processing (libav) module.
 */


/**
 * Implement hook_rules_action_info().
 */
function libav_rules_action_info() {
  $actions = array();
  $actions['libav_action_calculate_duration'] = array(
    'label' => t('Calculate audio/video duration'),
    'group' => t('Media'),
    'parameter' => array(
      'source_file' => array(
        'type' => 'file',
        'label' => t('Source audio/video file'),
      ),
    ),
    'provides' => array(
      'duration' => array(
        'type' => 'integer',
        'label' => t('Audio/Video duration'),
      ),
    ),
  );
  $actions['libav_action_audio_conversion'] = array(
    'label' => t('Audio conversion'),
    'group' => t('Media'),
    'parameter' => array(
      'source_file' => array(
        'type' => 'file',
        'label' => t('Source audio/video file'),
      ),
      'audio_preset' => array(
        'type' => 'text',
        'label' => t('Audio conversion preset'),
        'options list' => '_libav_audio_conversion_presets',
      ),
    ),
    'provides' => array(
      'converted_audio_file' => array(
        'type' => 'file',
        'label' => t('Converted audio file'),
      ),
    ),
  );
  $actions['libav_action_video_conversion'] = array(
    'label' => t('Video conversion'),
    'group' => t('Media'),
    'parameter' => array(
      'source_file' => array(
        'type' => 'file',
        'label' => t('Source video file'),
      ),
      'video_preset' => array(
        'type' => 'text',
        'label' => t('Video conversion preset'),
        'options list' => '_libav_video_conversion_presets',
      ),
    ),
    'provides' => array(
      'converted_video_file' => array(
        'type' => 'file',
        'label' => t('Converted video file'),
      ),
    ),
  );
  $actions['libav_action_video_thumbnail'] = array(
    'label' => t('Create video thumbnail'),
    'group' => t('Media'),
    'parameter' => array(
      'source_file' => array(
        'type' => 'file',
        'label' => t('Source video file'),
      ),
      'time_offset' => array(
        'type' => 'integer',
        'label' => t('Time offset'),
      ),
    ),
    'provides' => array(
      'video_thumbnail' => array(
        'type' => 'file',
        'label' => t('Video thumbnail'),
      ),
    ),
  );
  return $actions;
}

/**
 * Implementation of libav_action_audio_conversion
 * @param $source_file
 * @param $audio_preset
 * @return array
 * @throws \RulesEvaluationException
 */
function libav_action_audio_conversion($source_file, $audio_preset) {
  $dest_extension = '';
  $output_args = '';
  // Check $source_file->type
  if (!(($source_file->type === 'video') || ($source_file->type === 'audio'))) {
    throw new RulesEvaluationException(t('Source file is not audio/video'));
  }

  // Audio conversion preset
  switch ($audio_preset) {
    case 'mp3':
      $output_args = '-acodec mp3 -f mp3';
      $dest_extension = '.mp3';
      break;
    case 'ogg':
      $output_args = '-acodec vorbis -strict -2 -f ogg';
      $dest_extension = '.ogg';
      break;
  }

  // Audio conversion
  $converted_audio_file = _libav_convert($source_file, NULL, $dest_extension, NULL, $output_args);
  if (!$converted_audio_file) {
    throw new RulesEvaluationException(t('Audio conversion failed.'));
  }

  return array('converted_audio_file' => $converted_audio_file);
}

/**
 * Implementation of libav_action_video_conversion
 * @param $source_file
 * @param $video_preset
 * @return array
 * @throws \RulesEvaluationException
 */
function libav_action_video_conversion($source_file, $video_preset) {
  $dest_extension = '';
  $output_args = '';
  // Check $source_file->type
  if ($source_file->type !== 'video') {
    throw new RulesEvaluationException(t('Source file is not video'));
  }

  // Video conversion preset
  switch ($video_preset) {
    case 'mp4':
      $output_args = '-vcodec h264 -acodec aac -strict -2 -f mp4';
      $dest_extension = '.mp4';
      break;
    case 'ogv':
      $output_args = '-vcodec libtheora -acodec vorbis -strict -2 -f ogg';
      $dest_extension = '.ogv';
      break;
    case 'webm':
      $output_args = '-vcodec vp8 -acodec vorbis -strict -2 -f webm';
      $dest_extension = '.webm';
      break;
  }

  // Audio conversion
  $converted_video_file = _libav_convert($source_file, NULL, $dest_extension, NULL, $output_args);
  if (!$converted_video_file) {
    throw new RulesEvaluationException(t('Video conversion failed.'));
  }

  return array('converted_video_file' => $converted_video_file);
}

/**
 * Implementation of libav_action_video_duration().
 * @param \stdClass $source_file
 * @return array
 * @throws \RulesEvaluationException
 */
function libav_action_calculate_duration(stdClass $source_file) {
  $command_args = '';

  // Source audio/video file
  $source_path = drupal_realpath($source_file->uri);
  if (is_file($source_path)) {
    $command_args .= '-i ' . escapeshellarg($source_path);
  }
  else {
    throw new RulesEvaluationException(t('Invalid source file: !path', array('!path' => $source_path)));
  }
  // Check $source_file->type
  if (!(($source_file->type === 'video') || ($source_file->type === 'audio'))) {
    throw new RulesEvaluationException(t('Source file is not audio/video'));
  }

  // Execute avconv
  $output = '';
  $error = '';
  _libav_avconv_exec($command_args, $output, $error);

  // Extract duration
  if (preg_match('/.*Duration: (\d{2}):(\d{2}):(\d{2}).*/', $error, $matches)) {
    $duration = (($matches[1] * 60) + $matches[2]) * 60 + $matches[3];
  }
  else {
    throw new RulesEvaluationException(t('Could not calculate audio/video duration'));
  }

  return array('duration' => $duration);
}

/**
 * Implementation of libav_action_video_thumbnail().
 * @param $source_file
 * @param $time_offset
 * @return array
 * @throws \RulesEvaluationException
 */
function libav_action_video_thumbnail($source_file, $time_offset) {
  // Check $source_file->type
  if ($source_file->type !== 'video') {
    throw new RulesEvaluationException(t('Source file is not video'));
  }

  // Create the video thumbnail
  $input_args = '-ss ' . $time_offset;
  $output_args = '-vframes 1';
  $video_thumbnail = _libav_convert($source_file, NULL, '.jpg', $input_args, $output_args);
  if ($video_thumbnail == FALSE) {
    throw new RulesEvaluationException(t('Video thumbnail creation failed.'));
  }

  return array('video_thumbnail' => $video_thumbnail);
}
