<?php
/**
 * @file
 * File administration and module settings UI for Libav audio/video processing (libav) module.
 */

/**
 * Libav settings form.
 */
function libav_settings_form() {
  $form = array();

  // avconv path
  $form['libav_avconv_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the converter binary'),
    '#default_value' => variable_get('libav_avconv_path', 'ffmpeg'),
    '#required' => TRUE,
    '#element_validate' => array('libav_element_validate_avconv_path'),
    '#description' => t('The complete path and filename of converter binary. For example: /usr/bin/avconv. Both avconv and ffmpeg are supported.'),
  );
  // Prepare sub-element to output version or errors.
  $form['version'] = array();
  $form['#after_build'] = array('_libav_build_version');

  return system_settings_form($form);
}

/**
 * Form element validation handler for avconv path setting.
 * @param $element
 * @param $form_state
 */
function libav_element_validate_avconv_path($element, &$form_state) {
  $status = _libav_check_avconv_path($element['#value']);
  if ($status['errors']) {
    form_error($element, implode('<br />', $status['errors']));
  }
}

/**
 * Verifies file path of converter binary by checking its version.
 *
 * @param $avconv_path
 *   The user-submitted file path to the converter executable.
 * @return array An associative array containing:
 * An associative array containing:
 * - output: The shell output of 'avconv -version', if any.
 * - errors: A list of error messages indicating whether avconv could not
 * be found or executed.
 */
function _libav_check_avconv_path($avconv_path) {
  $status = array(
    'output' => '',
    'errors' => array(),
  );

  // If only the name of the executable is given, we only check whether it is in
  // the path and can be invoked.
  if ($avconv_path != 'avconv') {
    // Check whether the given file exists.
    if (!is_file($avconv_path)) {
      $status['errors'][] = t('The specified converter file path %file does not exist.', array('%file' => $avconv_path));
    } // If it exists, check whether we can execute it.
    elseif (!is_executable($avconv_path)) {
      $status['errors'][] = t('The specified converter file path %file is not executable.', array('%file' => $avconv_path));
    }
  }
  // In case of errors, check for open_basedir restrictions.
  if ($status['errors'] && ($open_basedir = ini_get('open_basedir'))) {
    $status['errors'][] = t('The PHP <a href="@php-url">open_basedir</a> security restriction is set to %open-basedir, which may prevent to locate avconv.', array(
      '%open-basedir' => $open_basedir,
      '@php-url' => 'http://php.net/manual/en/ini.core.php#ini.open-basedir',
    ));
  }

  // Unless we had errors so far, try to invoke avconv.
  if (!$status['errors']) {
    $error = '';
    _libav_avconv_exec('-version', $status['output'], $error, $avconv_path);
    // _libav_avconv_exec() triggers a user error upon failure, but
    // during form validation all errors need to be reported.
    if (strpos($error, 'Error') == FALSE) {
      $status['output'] = $error;
    }
    elseif ($error !== '') {
      // $error normally needs check_plain(), but file system errors on Windows
      // use a unknown encoding. check_plain() would eliminate the entire string.
      $status['errors'][] = $error;
    }
  }

  return $status;
}

/**
 * #after_build callback to output converter version or any errors in Libav settings form.
 * @param $element
 * @param $form_state
 * @return
 */
function _libav_build_version($element, &$form_state) {
  // Do not attempt to output version information when the form is submitted.
  if ($form_state['process_input']) {
    return $element;
  }
  // When the form is not submitted and only rendered, attempt to output version
  // information.
  $status = _libav_check_avconv_path($form_state['values']['libav_avconv_path']);
  if ($status['errors']) {
    $element['version'] = array(
      '#markup' => '<p class="error">' . implode('<br />', $status['errors']) . '</p>',
    );
  }
  else {
    $element['version'] = array(
      '#type' => 'item',
      '#title' => t('Version information'),
      '#weight' => 10,
      '#markup' => '<pre>' . check_plain(trim($status['output'])) . '</pre>',
      '#description' => t('Converter was found and returns this version information.'),
    );
  }
  return $element;
}
